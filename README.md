# README #

## NO LONGER UPDATED ##
This has been incorporated into the EDS-Complete repository and any further updates will be done there.

* * *

## What this does ##

When inserted into EBSCOhost bottom branding, this moves the EDS RTAC table from the end of the metadata fields to above them.

Making the user scroll to see holdings is plainly bad usability, so placement of the RTAC table on detailed record screens is a major drawback in EDS. This should be better.

![holdings table above title](https://bitbucket.org/repo/Rj6pgM/images/1859502645-rtac-top.png)

What would be better still would be to insert the RTAC table somewhere not far below the item title. However, these fields are marked up as a definition list, so you you can't insert a **section** element in there and still have valid markup. 

We're putting it in the bottom branding rather than a detailed record widget to ensure it happens quickly, for minimal disruption. Detailed record widgets load after the main content.

## Dependencies ##

* If you have multiple holdings, this can cause a problem because the item metadata gets pushed too far down the page. So we are using [an additional script](https://bitbucket.org/los-rios-libraries/eds-adjust-rtac-table-on-detailed-record-screens) to address that issue.
* The Holdings widget on the left-hand side is no longer needed when this script is used, so in any CSS the following can be used: `#rtac_panel {display:none;}`